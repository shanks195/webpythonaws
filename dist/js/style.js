/*for frontend==============================================================*/

$(window).ready(function () {
    collapseSidebar();
    headerAction();
    toggleSwitch();
    login();
    slider();
    $('.js-datepicker').datepicker();
    $(".js-seclect2").select2();
    if ($('#particles-js').length) {
        particlesJS("particles-js", optionsParticle);
    }

});

//Sidebar
function collapseSidebar() {
    //icon open header on mobile
    $('.header_text_center .icon').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active')
            $('.user').removeClass('active')
        } else {
            $(this).addClass('active')
            $('.user').addClass('active')
        }
    })

    //icon bar show/hide sidebar
    $('.header__toggle_icon').click(function () {
        if ($('.main_wrap').hasClass('hide')) {
            $('.main_wrap').removeClass('hide')
        } else {
            $('.main_wrap').addClass('hide')
        }
    })
    //icon bar show/hide sidebar mobie
    $('.sidebar-toggle').click(function () {
        if ($('.main_wrap').hasClass('hide')) {
            $('.main_wrap').removeClass('hide')
        } else {
            $('.main_wrap').addClass('hide')
        }
    })

    //click item on sidebar
    $('.nav-link').click(function () {
        if ($(this).hasClass('active')) {
            $(this).closest('.nav').find('.nav-item').removeClass('active')
        } else {
            $(this).closest('.nav-item').addClass('active')
        }
    })
}

//header action
function headerAction() {
    //click show dropdown user
    $('.user__wrap').click(function () {
        $('.user__actions').toggleClass('active')
    })
    $('.header-toggle').click(function () {
        $('.sidebar').toggleClass('active')
        $(this).toggleClass('active')
    })

}

// -----------------login----------------
function login() {
    $('.input-icon').click(function () {
        let input = $('.password').find(".form-control");
        $(this).toggleClass("fa-eye fa-eye-slash");
        input.attr('type') === 'password' ? input.attr('type', 'text') : input.attr('type', 'password');
    });
}

//-------------toggleSwitch----------------

function toggleSwitch() {
    $('.custom-switch').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active')
        } else {
            $(this).addClass('active')
        }
    })
}
//-------------tab----------------

$(document).ready(function () {
    function activeTab(obj) {
        $('.tab-wrapper ul li').removeClass('active');
        $(obj).addClass('active');
        var id = $(obj).find('a').attr('href');
        $('.tab-pane').hide();
        $(id).show();
    }
    $('.nav-tabs li').click(function () {
        activeTab(this);
        return false;
    });
    activeTab($('.nav-tabs li:first-child'));
});
//-------------particlesJS----------------

var optionsParticle = {
    particles: {
        number: { value: 80, density: { enable: true, value_area: 800 } },
        color: { value: "#ffffff" },
        shape: {
            type: "circle",
            stroke: { width: 0, color: "#000000" },
            polygon: { nb_sides: 5 },
            image: { src: "img/github.svg", width: 100, height: 100 }
        },
        opacity: {
            value: 0.5,
            random: false,
            anim: { enable: false, speed: 1, opacity_min: 0.1, sync: false }
        },
        size: {
            value: 3,
            random: true,
            anim: { enable: false, speed: 40, size_min: 0.1, sync: false }
        },
        line_linked: {
            enable: true,
            distance: 150,
            color: "#ffffff",
            opacity: 0.4,
            width: 1
        },
        move: {
            enable: true,
            speed: 6,
            direction: "none",
            random: false,
            straight: false,
            out_mode: "out",
            bounce: false,
            attract: { enable: false, rotateX: 600, rotateY: 1200 }
        }
    },
    interactivity: {
        detect_on: "canvas",
        events: {
            onhover: { enable: true, mode: "repulse" },
            onclick: { enable: true, mode: "push" },
            resize: true
        },
        modes: {
            grab: { distance: 400, line_linked: { opacity: 1 } },
            bubble: { distance: 400, size: 40, duration: 2, opacity: 8, speed: 3 },
            repulse: { distance: 200, duration: 0.4 },
            push: { particles_nb: 4 },
            remove: { particles_nb: 2 }
        }
    },
    retina_detect: true
}
//-------------particlesJS----------------
function slider() {
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav',
        autoplay: false
    });
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        infinite: true,
        centerPadding: '0px',
        prevArrow: '<i class="las la-angle-left icon-left"></i>',
        nextArrow: '<i class="las la-angle-right icon-right"></i>',
        responsive: [{
            breakpoint: 991,
            settings: {
                slidesToShow: 4,
            }
        }]
    });

}
