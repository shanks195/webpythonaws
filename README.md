
# Source HTML

# - master : don't push anything on here!!!!

# - all banches
- html-scb (html)
- html-ekyc (html)
- html-ekyc-v2 (html)
- html-uni-sales (html -> for uniprime and sales admin)
- Sunny_main (html -> superapp/main-web)
- Sunny_test (test reactjs on main-web)
- Uniprime_landing (html -> uniprime landing page - a Loc)
- Sunny_sub (html -> superapp fullpage)
- Uniprime Chat Only (html)
- smarthome_landing (html landing page)
- sale_landing (html landing page)
- CCTV (html)
- react-grid-layout (react for test libs)
- react_structure (react structure template)

# - Run source on Webpack
- npm i (npm install)
- npm start
- add a new file -> 'npm start' again (or 'npm run build')


# RULE FOR ALL
1. Làm theo đúng mẫu Example, nếu cần thay đổi hãy request to leader.
2. Commit status rõ ràng.
3. Pull and Push code mỗi ngày để cập nhật code mới nhất. 17h mỗi ngày là bắt buộc.
4. Tắt console log khi đã push code và khi đã done module.
5. Nếu muốn cập nhật component dùng chung, hãy tham khảo qua ý kiến leader.