# REACT SALE ADMIN 

````
# Readmore for source and rule react
````

# Command 
- On run:
    + npm start : on start
    + npm run build-dev/build-sta/build-prod : build for env (dùng file env )
    + npm run build-domain -- --domain=xxx : build với domain xxx, ko dùng file env
- On clear: 
    + npm run clean :  xóa build
    + npm run clean-dev/clean-sta/clean-prod/clean-domain :  xóa các file build tương ứng với tên

# Source Detail

* env: include domain for run
* locales -> translate
* public -> assets and style all
* server -> config for proxy
* src
    - App: config layout and route --> config đầu vào, check auth...

    - components:
        + base: element html (from library:antd, material, bootstrap...) --> những component dùng chung lấy từ thư viện
        + common : element layout --> những layout dùng chung và tự custom

    - data: fake data json/js

    - functions:
        + Utils.js: những func đc tái sử dụng và có thể dùng nhiều nơi, ko chứa biến riêng của layout nào
        + ServiceHandle: xử lý handle request/response (fetch, axios...)

    - polyfills: config build on browser enviroment (IE, Safari...) and something for compatibility browser

    - constant : chứa những value dùng chung và data constant 

    - realtime: rabbit, socket...

    - router:
        + routesMain : route redirect for app
        + routesUrlMenu : url for menu

    - services: những file service đc tách ra theo từng folder
        + api: only link api

    - store: 
        + Quản lý theo folder
        + Mỗi folder chứa (action + reducer + saga) 

    - templates: 
        + Cấu trúc template:
            - folder 'layout': chứa những component con của page
            - page : Những file nằm cùng cấp 'layout'
            - ...

* webpack:
    - common.js --> config all.
    - dev, prod, sta --> run code by domain on env
    - domain --> run code by domain when command 'npm run build-domain -- --domain=xxxx'


# Quy tắc đặt tên 
````````````
camelCase 
PascalCase 
snake_case
kebab-case
````````````

Danh từ/tên định danh + action (cho cả tên file, folder and variable translation)
- Example :   
    + status_update_success
    + status_create_success
    + status_create_fail
    + status_create_fail
    + database_create
    + database_update

- folder structure ( never change : env/locales/public/server/src/app/components/templates...) -> snake_case
- img, css/scss/sass: name_name.type (logo_text.png)
- file/folder in js: PascalCase -> PascalCase/PascalCase.type (Profile.js)


# RULE FOR ALL
````
1. Làm theo đúng mẫu Example, nếu cần thay đổi hãy request to leader.
2. Commit status rõ ràng.
3. Pull and Push code mỗi ngày để cập nhật code mới nhất. 17h mỗi ngày là bắt buộc.
4. Tắt console log khi đã push code và khi đã done module.
5. Nếu muốn cập nhật component dùng chung, hãy tham khảo qua ý kiến leader.

````

# LIB:
- https://visjs.github.io/vis-timeline/examples/timeline/
- https://github.com/STRML/react-grid-layout
- https://www.twilio.com/blog/npm-scripts --> for npm


````
Code for fun!
(*Trình độ và kỹ năng không quan trọng bằng thái độ*)
````